﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Xml.XPath;

namespace EndlessLegendSaveEditor
{

    public partial class HeroEditForm : Form // Main form for editing heroes
    {
        public List<HeroEntry> HeroList = new List<HeroEntry>(); // List of heroes 

        public HeroEditForm(IList<HeroEntry> heroList) // Constructor, defaults
        {
            InitializeComponent();
            SetHeroList(heroList);
        }

        public void SetHeroList(IList<HeroEntry> newhlist)
        { // Sets hero to have entries same as in newhlist
            HeroList.Clear();
            HeroList.AddRange(newhlist);
            HeroListUpdated(); // Refresh hero names list box
        }

        public bool HeroDataChanged()
        { // Returns true if any hero data has been changed by user
            foreach (HeroEntry hero in HeroList)
                if (hero.changed) return true;
            return false;
        }

        private void btClose_Click(object sender, EventArgs e) // Close button pressed, exits with OK modal result
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void HeroListUpdated()
        { // Refreshes hero list box to current list
            lbHeroeNames.Items.Clear();
            foreach (HeroEntry hero in HeroList)
                lbHeroeNames.Items.Add(hero.Name);
            if (lbHeroeNames.Items.Count > 0)
                lbHeroeNames.SelectedIndex = 0;
        }

        private void lbHeroeNames_SelectedIndexChanged(object sender, EventArgs e) // Selected item in hero list box has changed, updates property grid
        {
            pgProperties.SelectedObject = HeroList[lbHeroeNames.SelectedIndex]; // Update property grid to display currently selected heroes entry
        }
    }

    public class HeroEntry : Object // Hero data object, for displaying in Property grid, acts as middle layer between XML data and Property Grid
    {
        public HeroEntry(XElement dnode, XElement unode, XDocument localizationXML) // Constructor, must be passed 2 xml nodes - dnode = Design Node, unode = Unit Node
        {
            // Note: Unit node attribute "UnitDesignModel" and UnitDesign node attribute "Model" are linked (must match), this way two of them are recognised as a pair
            designNode = dnode; // Design node in XML file stores Hero name and some other data
            unitNode = unode; // Unit node in XML file stores hero property like XP data
            locXML = localizationXML; // Localiation used to resolve hero names if not null
        }

        public bool changed = false; // State flag, true if any hero property was edited.

        public XElement designNode;
        public XElement unitNode;
        public XDocument locXML = null;

        private string GetLocString(string key) { // Get Localized string
            if (locXML != null) {
                XElement locNode = locXML.XPathSelectElement("/Datatable/LocalizationPair[@Name='" + key + "']");
                if (locNode != null) return locNode.Value;
            }
            return "";
        }

        private string GetProperty(string propname)
        { // helper function, returns property by name <propname> or empty string if no such property in XML unit node
            XElement property = unitNode.XPathSelectElement("SimulationObject/Properties/Property[@Name='" + propname + "']");
            if (property != null) return property.Value;
            return "";
        }

        private void SetProperty(string propname, string newvalue)
        { // helper function, stores property value <newvalue> to property <propname>, shows error message if XML node has no such property
            XElement property = unitNode.XPathSelectElement("SimulationObject/Properties/Property[@Name='" + propname + "']");
            if (property != null)
            {
                property.Value = newvalue;
                changed = true;
            }
            else
            {
                MessageBox.Show("Hero has no property named \"" + propname + "\", for example if hero has just been bought and has no experience yet.", "Setting Invalid Property");
            }
        }

        // Properties for using with Property grid, change or add those to modify how properties are displayed and changed
        [DisplayName("Hero Name"), CategoryAttribute("Names"), DescriptionAttribute("Heroe Name"), ReadOnlyAttribute(true)]
        public string Name
        {
            get {
                XElement uname = designNode.Element("UserDefinedName");
                if (uname != null) 
                    return uname.Value;
                else {
                    string hero_name = GetLocString(designNode.Element("LocalizationKey").Value);
                    if (hero_name != "") return hero_name;
                    else return designNode.Attribute("Name").Value;
                }
            }
        }

        [DisplayName("Skill Points"), CategoryAttribute("Properties")]
        public int MaximumSkillPoints
        {
            get { return Convert.ToInt32(GetProperty("MaximumSkillPoints")); }
            set { SetProperty("MaximumSkillPoints", value.ToString()); }
        }

        [DisplayName("Experience"), CategoryAttribute("Properties")]
        public float Experience
        {
            get
            {
                try
                {
                    return Convert.ToSingle(GetProperty("Experience"));
                }
                catch (FormatException)
                {
                    return 0.0f;
                }
            }
            set { SetProperty("Experience", value.ToString()); }
        }
    }
}