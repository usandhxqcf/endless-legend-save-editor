﻿namespace EndlessLegendSaveEditor
{
  partial class MainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.tbDust = new System.Windows.Forms.TextBox();
            this.tbEmpirePoints = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.cbBackup = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbResearchStock = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbPeacePoints = new System.Windows.Forms.TextBox();
            this.cbStrategicResources = new System.Windows.Forms.CheckBox();
            this.cbLuxuryResources = new System.Windows.Forms.CheckBox();
            this.cbHealAllArmies = new System.Windows.Forms.CheckBox();
            this.cbBoostersUp = new System.Windows.Forms.CheckBox();
            this.cbResetSpentMovement = new System.Windows.Forms.CheckBox();
            this.cbUnlimitedMovementForever = new System.Windows.Forms.CheckBox();
            this.cbUnlimitedMovementOneTurn = new System.Windows.Forms.CheckBox();
            this.cbHeroSkillPoint = new System.Windows.Forms.CheckBox();
            this.cbResearchProgress = new System.Windows.Forms.CheckBox();
            this.cbInstantPlan = new System.Windows.Forms.CheckBox();
            this.cbUnlockHeroAssign = new System.Windows.Forms.CheckBox();
            this.cbUnlimitedActionPoints = new System.Windows.Forms.CheckBox();
            this.tlpBottom = new System.Windows.Forms.TableLayoutPanel();
            this.gbResources = new System.Windows.Forms.GroupBox();
            this.tlpEmpireManagement = new System.Windows.Forms.TableLayoutPanel();
            this.flpResources = new System.Windows.Forms.FlowLayoutPanel();
            this.cbEnemyResources = new System.Windows.Forms.CheckBox();
            this.gbEmpireManagement = new System.Windows.Forms.GroupBox();
            this.flpEmpireManagement = new System.Windows.Forms.FlowLayoutPanel();
            this.cbCityPop = new System.Windows.Forms.CheckBox();
            this.gbUnitsAndHeroes = new System.Windows.Forms.GroupBox();
            this.flpUnitsAndHeroes = new System.Windows.Forms.FlowLayoutPanel();
            this.cbHeroExp = new System.Windows.Forms.CheckBox();
            this.cbHeroAbilityLvMax = new System.Windows.Forms.CheckBox();
            this.btnEditHeroes = new System.Windows.Forms.Button();
            this.flpOperation = new System.Windows.Forms.FlowLayoutPanel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btnReloadSaveList = new System.Windows.Forms.Button();
            this.lbSaveFiles = new System.Windows.Forms.ListBox();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.gbSaveFile = new System.Windows.Forms.GroupBox();
            this.statusStrip1.SuspendLayout();
            this.tlpBottom.SuspendLayout();
            this.gbResources.SuspendLayout();
            this.tlpEmpireManagement.SuspendLayout();
            this.flpResources.SuspendLayout();
            this.gbEmpireManagement.SuspendLayout();
            this.flpEmpireManagement.SuspendLayout();
            this.gbUnitsAndHeroes.SuspendLayout();
            this.flpUnitsAndHeroes.SuspendLayout();
            this.flpOperation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.gbSaveFile.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbDust
            // 
            this.tbDust.Location = new System.Drawing.Point(3, 25);
            this.tbDust.Name = "tbDust";
            this.tbDust.Size = new System.Drawing.Size(126, 20);
            this.tbDust.TabIndex = 1;
            // 
            // tbEmpirePoints
            // 
            this.tbEmpirePoints.Location = new System.Drawing.Point(214, 25);
            this.tbEmpirePoints.Name = "tbEmpirePoints";
            this.tbEmpirePoints.Size = new System.Drawing.Size(126, 20);
            this.tbEmpirePoints.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Dust:";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(214, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Empire Points:";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnUpdate.Location = new System.Drawing.Point(3, 3);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(188, 24);
            this.btnUpdate.TabIndex = 5;
            this.btnUpdate.Text = "Update Save";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 659);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(434, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // cbBackup
            // 
            this.cbBackup.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.cbBackup.AutoSize = true;
            this.cbBackup.Checked = true;
            this.cbBackup.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbBackup.Location = new System.Drawing.Point(197, 6);
            this.cbBackup.Name = "cbBackup";
            this.cbBackup.Size = new System.Drawing.Size(63, 17);
            this.cbBackup.TabIndex = 8;
            this.cbBackup.Text = "Backup";
            this.cbBackup.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Research Stock:";
            // 
            // tbResearchStock
            // 
            this.tbResearchStock.Location = new System.Drawing.Point(3, 80);
            this.tbResearchStock.Name = "tbResearchStock";
            this.tbResearchStock.Size = new System.Drawing.Size(126, 20);
            this.tbResearchStock.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(214, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Peace Points:";
            // 
            // tbPeacePoints
            // 
            this.tbPeacePoints.Location = new System.Drawing.Point(214, 80);
            this.tbPeacePoints.Name = "tbPeacePoints";
            this.tbPeacePoints.Size = new System.Drawing.Size(126, 20);
            this.tbPeacePoints.TabIndex = 12;
            // 
            // cbStrategicResources
            // 
            this.cbStrategicResources.AutoSize = true;
            this.cbStrategicResources.Location = new System.Drawing.Point(3, 3);
            this.cbStrategicResources.Name = "cbStrategicResources";
            this.cbStrategicResources.Size = new System.Drawing.Size(179, 17);
            this.cbStrategicResources.TabIndex = 13;
            this.cbStrategicResources.Text = "Strategic Resources ( 6 )  to 999";
            this.cbStrategicResources.UseVisualStyleBackColor = true;
            // 
            // cbLuxuryResources
            // 
            this.cbLuxuryResources.AutoSize = true;
            this.cbLuxuryResources.Location = new System.Drawing.Point(188, 3);
            this.cbLuxuryResources.Name = "cbLuxuryResources";
            this.cbLuxuryResources.Size = new System.Drawing.Size(180, 17);
            this.cbLuxuryResources.TabIndex = 14;
            this.cbLuxuryResources.Text = "Luxury Resources   ( 15 )  to 999";
            this.cbLuxuryResources.UseVisualStyleBackColor = true;
            // 
            // cbHealAllArmies
            // 
            this.cbHealAllArmies.AutoSize = true;
            this.cbHealAllArmies.Location = new System.Drawing.Point(3, 3);
            this.cbHealAllArmies.Name = "cbHealAllArmies";
            this.cbHealAllArmies.Size = new System.Drawing.Size(96, 17);
            this.cbHealAllArmies.TabIndex = 15;
            this.cbHealAllArmies.Text = "Heal All Armies";
            this.cbHealAllArmies.UseVisualStyleBackColor = true;
            // 
            // cbBoostersUp
            // 
            this.cbBoostersUp.AutoSize = true;
            this.cbBoostersUp.Location = new System.Drawing.Point(3, 3);
            this.cbBoostersUp.Name = "cbBoostersUp";
            this.cbBoostersUp.Size = new System.Drawing.Size(178, 17);
            this.cbBoostersUp.TabIndex = 16;
            this.cbBoostersUp.Text = "Set All Boosters to last 600 turns";
            this.cbBoostersUp.UseVisualStyleBackColor = true;
            // 
            // cbResetSpentMovement
            // 
            this.cbResetSpentMovement.AutoSize = true;
            this.cbResetSpentMovement.Location = new System.Drawing.Point(3, 49);
            this.cbResetSpentMovement.Name = "cbResetSpentMovement";
            this.cbResetSpentMovement.Size = new System.Drawing.Size(218, 17);
            this.cbResetSpentMovement.TabIndex = 17;
            this.cbResetSpentMovement.Text = "Reset All Armies Spent Movement Points";
            this.cbResetSpentMovement.UseVisualStyleBackColor = true;
            // 
            // cbUnlimitedMovementForever
            // 
            this.cbUnlimitedMovementForever.AutoSize = true;
            this.cbUnlimitedMovementForever.Location = new System.Drawing.Point(105, 3);
            this.cbUnlimitedMovementForever.Name = "cbUnlimitedMovementForever";
            this.cbUnlimitedMovementForever.Size = new System.Drawing.Size(237, 17);
            this.cbUnlimitedMovementForever.TabIndex = 19;
            this.cbUnlimitedMovementForever.Text = "All Units Unlimited Movement Points(Forever)";
            this.cbUnlimitedMovementForever.UseVisualStyleBackColor = true;
            // 
            // cbUnlimitedMovementOneTurn
            // 
            this.cbUnlimitedMovementOneTurn.AutoSize = true;
            this.cbUnlimitedMovementOneTurn.Location = new System.Drawing.Point(3, 26);
            this.cbUnlimitedMovementOneTurn.Name = "cbUnlimitedMovementOneTurn";
            this.cbUnlimitedMovementOneTurn.Size = new System.Drawing.Size(246, 17);
            this.cbUnlimitedMovementOneTurn.TabIndex = 20;
            this.cbUnlimitedMovementOneTurn.Text = "All Units Unlimited Movement Points(One Turn)";
            this.cbUnlimitedMovementOneTurn.UseVisualStyleBackColor = true;
            // 
            // cbHeroSkillPoint
            // 
            this.cbHeroSkillPoint.AutoSize = true;
            this.cbHeroSkillPoint.Location = new System.Drawing.Point(201, 72);
            this.cbHeroSkillPoint.Name = "cbHeroSkillPoint";
            this.cbHeroSkillPoint.Size = new System.Drawing.Size(178, 17);
            this.cbHeroSkillPoint.TabIndex = 21;
            this.cbHeroSkillPoint.Text = "All Heroes Have 120 Skill Points";
            this.cbHeroSkillPoint.UseVisualStyleBackColor = true;
            // 
            // cbResearchProgress
            // 
            this.cbResearchProgress.AutoSize = true;
            this.cbResearchProgress.Location = new System.Drawing.Point(187, 3);
            this.cbResearchProgress.Name = "cbResearchProgress";
            this.cbResearchProgress.Size = new System.Drawing.Size(200, 17);
            this.cbResearchProgress.TabIndex = 22;
            this.cbResearchProgress.Text = "All Queued Research Progress 100%";
            this.cbResearchProgress.UseVisualStyleBackColor = true;
            // 
            // cbInstantPlan
            // 
            this.cbInstantPlan.AutoSize = true;
            this.cbInstantPlan.Location = new System.Drawing.Point(3, 26);
            this.cbInstantPlan.Name = "cbInstantPlan";
            this.cbInstantPlan.Size = new System.Drawing.Size(175, 17);
            this.cbInstantPlan.TabIndex = 23;
            this.cbInstantPlan.Text = "Allow Change Empire Plan Now";
            this.cbInstantPlan.UseVisualStyleBackColor = true;
            // 
            // cbUnlockHeroAssign
            // 
            this.cbUnlockHeroAssign.AutoSize = true;
            this.cbUnlockHeroAssign.Location = new System.Drawing.Point(165, 95);
            this.cbUnlockHeroAssign.Name = "cbUnlockHeroAssign";
            this.cbUnlockHeroAssign.Size = new System.Drawing.Size(145, 17);
            this.cbUnlockHeroAssign.TabIndex = 24;
            this.cbUnlockHeroAssign.Text = "Unlock Hero Assign Now";
            this.cbUnlockHeroAssign.UseVisualStyleBackColor = true;
            // 
            // cbUnlimitedActionPoints
            // 
            this.cbUnlimitedActionPoints.AutoSize = true;
            this.cbUnlimitedActionPoints.Location = new System.Drawing.Point(227, 49);
            this.cbUnlimitedActionPoints.Name = "cbUnlimitedActionPoints";
            this.cbUnlimitedActionPoints.Size = new System.Drawing.Size(175, 17);
            this.cbUnlimitedActionPoints.TabIndex = 25;
            this.cbUnlimitedActionPoints.Text = "All Units Unlimited Action Points";
            this.cbUnlimitedActionPoints.UseVisualStyleBackColor = true;
            // 
            // tlpBottom
            // 
            this.tlpBottom.ColumnCount = 1;
            this.tlpBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpBottom.Controls.Add(this.gbResources, 0, 0);
            this.tlpBottom.Controls.Add(this.gbEmpireManagement, 0, 1);
            this.tlpBottom.Controls.Add(this.gbUnitsAndHeroes, 0, 2);
            this.tlpBottom.Controls.Add(this.flpOperation, 0, 3);
            this.tlpBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpBottom.Location = new System.Drawing.Point(0, 0);
            this.tlpBottom.Name = "tlpBottom";
            this.tlpBottom.RowCount = 4;
            this.tlpBottom.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpBottom.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 31.4003F));
            this.tlpBottom.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 68.59969F));
            this.tlpBottom.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tlpBottom.Size = new System.Drawing.Size(434, 477);
            this.tlpBottom.TabIndex = 26;
            // 
            // gbResources
            // 
            this.gbResources.Controls.Add(this.tlpEmpireManagement);
            this.gbResources.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbResources.Location = new System.Drawing.Point(3, 3);
            this.gbResources.Name = "gbResources";
            this.gbResources.Size = new System.Drawing.Size(428, 175);
            this.gbResources.TabIndex = 0;
            this.gbResources.TabStop = false;
            this.gbResources.Text = "Resources";
            // 
            // tlpEmpireManagement
            // 
            this.tlpEmpireManagement.ColumnCount = 2;
            this.tlpEmpireManagement.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpEmpireManagement.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpEmpireManagement.Controls.Add(this.label1, 0, 0);
            this.tlpEmpireManagement.Controls.Add(this.label2, 1, 0);
            this.tlpEmpireManagement.Controls.Add(this.tbDust, 0, 1);
            this.tlpEmpireManagement.Controls.Add(this.tbEmpirePoints, 1, 1);
            this.tlpEmpireManagement.Controls.Add(this.tbPeacePoints, 1, 3);
            this.tlpEmpireManagement.Controls.Add(this.label3, 0, 2);
            this.tlpEmpireManagement.Controls.Add(this.tbResearchStock, 0, 3);
            this.tlpEmpireManagement.Controls.Add(this.label4, 1, 2);
            this.tlpEmpireManagement.Controls.Add(this.flpResources, 0, 4);
            this.tlpEmpireManagement.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpEmpireManagement.Location = new System.Drawing.Point(3, 16);
            this.tlpEmpireManagement.Name = "tlpEmpireManagement";
            this.tlpEmpireManagement.RowCount = 5;
            this.tlpEmpireManagement.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tlpEmpireManagement.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tlpEmpireManagement.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tlpEmpireManagement.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tlpEmpireManagement.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpEmpireManagement.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tlpEmpireManagement.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tlpEmpireManagement.Size = new System.Drawing.Size(422, 156);
            this.tlpEmpireManagement.TabIndex = 0;
            // 
            // flpResources
            // 
            this.tlpEmpireManagement.SetColumnSpan(this.flpResources, 2);
            this.flpResources.Controls.Add(this.cbStrategicResources);
            this.flpResources.Controls.Add(this.cbLuxuryResources);
            this.flpResources.Controls.Add(this.cbEnemyResources);
            this.flpResources.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpResources.Location = new System.Drawing.Point(3, 113);
            this.flpResources.Name = "flpResources";
            this.flpResources.Size = new System.Drawing.Size(416, 108);
            this.flpResources.TabIndex = 16;
            // 
            // cbEnemyResources
            // 
            this.cbEnemyResources.AutoSize = true;
            this.cbEnemyResources.Location = new System.Drawing.Point(3, 26);
            this.cbEnemyResources.Name = "cbEnemyResources";
            this.cbEnemyResources.Size = new System.Drawing.Size(240, 17);
            this.cbEnemyResources.TabIndex = 15;
            this.cbEnemyResources.Text = "Enemy Cheating(value > 800) Resources to 0";
            this.cbEnemyResources.UseVisualStyleBackColor = true;
            // 
            // gbEmpireManagement
            // 
            this.gbEmpireManagement.Controls.Add(this.flpEmpireManagement);
            this.gbEmpireManagement.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbEmpireManagement.Location = new System.Drawing.Point(3, 184);
            this.gbEmpireManagement.Name = "gbEmpireManagement";
            this.gbEmpireManagement.Size = new System.Drawing.Size(428, 76);
            this.gbEmpireManagement.TabIndex = 2;
            this.gbEmpireManagement.TabStop = false;
            this.gbEmpireManagement.Text = "Empire Management";
            // 
            // flpEmpireManagement
            // 
            this.flpEmpireManagement.Controls.Add(this.cbBoostersUp);
            this.flpEmpireManagement.Controls.Add(this.cbResearchProgress);
            this.flpEmpireManagement.Controls.Add(this.cbInstantPlan);
            this.flpEmpireManagement.Controls.Add(this.cbCityPop);
            this.flpEmpireManagement.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpEmpireManagement.Location = new System.Drawing.Point(3, 16);
            this.flpEmpireManagement.Name = "flpEmpireManagement";
            this.flpEmpireManagement.Size = new System.Drawing.Size(422, 57);
            this.flpEmpireManagement.TabIndex = 0;
            // 
            // cbCityPop
            // 
            this.cbCityPop.AutoSize = true;
            this.cbCityPop.Location = new System.Drawing.Point(184, 26);
            this.cbCityPop.Name = "cbCityPop";
            this.cbCityPop.Size = new System.Drawing.Size(198, 17);
            this.cbCityPop.TabIndex = 24;
            this.cbCityPop.Text = "Increase City Population and Growth";
            this.cbCityPop.UseVisualStyleBackColor = true;
            // 
            // gbUnitsAndHeroes
            // 
            this.gbUnitsAndHeroes.Controls.Add(this.flpUnitsAndHeroes);
            this.gbUnitsAndHeroes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbUnitsAndHeroes.Location = new System.Drawing.Point(3, 266);
            this.gbUnitsAndHeroes.Name = "gbUnitsAndHeroes";
            this.gbUnitsAndHeroes.Size = new System.Drawing.Size(428, 174);
            this.gbUnitsAndHeroes.TabIndex = 3;
            this.gbUnitsAndHeroes.TabStop = false;
            this.gbUnitsAndHeroes.Text = "Units and Heroes";
            // 
            // flpUnitsAndHeroes
            // 
            this.flpUnitsAndHeroes.Controls.Add(this.cbHealAllArmies);
            this.flpUnitsAndHeroes.Controls.Add(this.cbUnlimitedMovementForever);
            this.flpUnitsAndHeroes.Controls.Add(this.cbUnlimitedMovementOneTurn);
            this.flpUnitsAndHeroes.Controls.Add(this.cbResetSpentMovement);
            this.flpUnitsAndHeroes.Controls.Add(this.cbUnlimitedActionPoints);
            this.flpUnitsAndHeroes.Controls.Add(this.cbHeroExp);
            this.flpUnitsAndHeroes.Controls.Add(this.cbHeroSkillPoint);
            this.flpUnitsAndHeroes.Controls.Add(this.cbHeroAbilityLvMax);
            this.flpUnitsAndHeroes.Controls.Add(this.cbUnlockHeroAssign);
            this.flpUnitsAndHeroes.Controls.Add(this.btnEditHeroes);
            this.flpUnitsAndHeroes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpUnitsAndHeroes.Location = new System.Drawing.Point(3, 16);
            this.flpUnitsAndHeroes.Name = "flpUnitsAndHeroes";
            this.flpUnitsAndHeroes.Size = new System.Drawing.Size(422, 155);
            this.flpUnitsAndHeroes.TabIndex = 0;
            // 
            // cbHeroExp
            // 
            this.cbHeroExp.AutoSize = true;
            this.cbHeroExp.Location = new System.Drawing.Point(3, 72);
            this.cbHeroExp.Name = "cbHeroExp";
            this.cbHeroExp.Size = new System.Drawing.Size(192, 17);
            this.cbHeroExp.TabIndex = 27;
            this.cbHeroExp.Text = "All Heroes Have 20000 Experience";
            this.cbHeroExp.UseVisualStyleBackColor = true;
            // 
            // cbHeroAbilityLvMax
            // 
            this.cbHeroAbilityLvMax.AutoSize = true;
            this.cbHeroAbilityLvMax.Location = new System.Drawing.Point(3, 95);
            this.cbHeroAbilityLvMax.Name = "cbHeroAbilityLvMax";
            this.cbHeroAbilityLvMax.Size = new System.Drawing.Size(156, 17);
            this.cbHeroAbilityLvMax.TabIndex = 28;
            this.cbHeroAbilityLvMax.Text = "All Heroes Ability Level Max";
            this.cbHeroAbilityLvMax.UseVisualStyleBackColor = true;
            // 
            // btnEditHeroes
            // 
            this.btnEditHeroes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEditHeroes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnEditHeroes.Location = new System.Drawing.Point(3, 118);
            this.btnEditHeroes.Name = "btnEditHeroes";
            this.btnEditHeroes.Size = new System.Drawing.Size(188, 24);
            this.btnEditHeroes.TabIndex = 26;
            this.btnEditHeroes.Text = "Edit Heroes";
            this.btnEditHeroes.UseVisualStyleBackColor = true;
            this.btnEditHeroes.Click += new System.EventHandler(this.btnEditHeroes_Click);
            // 
            // flpOperation
            // 
            this.flpOperation.Controls.Add(this.btnUpdate);
            this.flpOperation.Controls.Add(this.cbBackup);
            this.flpOperation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpOperation.Location = new System.Drawing.Point(3, 446);
            this.flpOperation.Name = "flpOperation";
            this.flpOperation.Size = new System.Drawing.Size(428, 28);
            this.flpOperation.TabIndex = 4;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(6, 19);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.btnReloadSaveList);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.lbSaveFiles);
            this.splitContainer1.Size = new System.Drawing.Size(419, 153);
            this.splitContainer1.SplitterDistance = 27;
            this.splitContainer1.TabIndex = 22;
            // 
            // btnReloadSaveList
            // 
            this.btnReloadSaveList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnReloadSaveList.Location = new System.Drawing.Point(0, 0);
            this.btnReloadSaveList.Name = "btnReloadSaveList";
            this.btnReloadSaveList.Size = new System.Drawing.Size(419, 27);
            this.btnReloadSaveList.TabIndex = 19;
            this.btnReloadSaveList.Text = "Refresh Save List";
            this.btnReloadSaveList.UseVisualStyleBackColor = true;
            // 
            // lbSaveFiles
            // 
            this.lbSaveFiles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbSaveFiles.FormattingEnabled = true;
            this.lbSaveFiles.Location = new System.Drawing.Point(0, 0);
            this.lbSaveFiles.Name = "lbSaveFiles";
            this.lbSaveFiles.ScrollAlwaysVisible = true;
            this.lbSaveFiles.Size = new System.Drawing.Size(419, 122);
            this.lbSaveFiles.TabIndex = 21;
            this.lbSaveFiles.SelectedIndexChanged += new System.EventHandler(this.lbSaveFiles_SelectedIndexChanged);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.gbSaveFile);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.tlpBottom);
            this.splitContainer2.Size = new System.Drawing.Size(434, 659);
            this.splitContainer2.SplitterDistance = 178;
            this.splitContainer2.TabIndex = 23;
            // 
            // gbSaveFile
            // 
            this.gbSaveFile.Controls.Add(this.splitContainer1);
            this.gbSaveFile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbSaveFile.Location = new System.Drawing.Point(0, 0);
            this.gbSaveFile.Name = "gbSaveFile";
            this.gbSaveFile.Size = new System.Drawing.Size(434, 178);
            this.gbSaveFile.TabIndex = 1;
            this.gbSaveFile.TabStop = false;
            this.gbSaveFile.Text = "Choose Save File";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 681);
            this.Controls.Add(this.splitContainer2);
            this.Controls.Add(this.statusStrip1);
            this.DoubleBuffered = true;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Endless Legend Save Editor";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tlpBottom.ResumeLayout(false);
            this.gbResources.ResumeLayout(false);
            this.tlpEmpireManagement.ResumeLayout(false);
            this.tlpEmpireManagement.PerformLayout();
            this.flpResources.ResumeLayout(false);
            this.flpResources.PerformLayout();
            this.gbEmpireManagement.ResumeLayout(false);
            this.flpEmpireManagement.ResumeLayout(false);
            this.flpEmpireManagement.PerformLayout();
            this.gbUnitsAndHeroes.ResumeLayout(false);
            this.flpUnitsAndHeroes.ResumeLayout(false);
            this.flpUnitsAndHeroes.PerformLayout();
            this.flpOperation.ResumeLayout(false);
            this.flpOperation.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.gbSaveFile.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox tbDust;
    private System.Windows.Forms.TextBox tbEmpirePoints;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Button btnUpdate;
    private System.Windows.Forms.StatusStrip statusStrip1;
    private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
    private System.Windows.Forms.CheckBox cbBackup;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox tbResearchStock;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.TextBox tbPeacePoints;
    private System.Windows.Forms.CheckBox cbStrategicResources;
    private System.Windows.Forms.CheckBox cbLuxuryResources;
    private System.Windows.Forms.CheckBox cbHealAllArmies;
    private System.Windows.Forms.CheckBox cbBoostersUp;
    private System.Windows.Forms.CheckBox cbResetSpentMovement;
    private System.Windows.Forms.CheckBox cbUnlimitedMovementForever;
    private System.Windows.Forms.CheckBox cbUnlimitedMovementOneTurn;
    private System.Windows.Forms.CheckBox cbHeroSkillPoint;
    private System.Windows.Forms.CheckBox cbResearchProgress;
    private System.Windows.Forms.CheckBox cbInstantPlan;
    private System.Windows.Forms.CheckBox cbUnlockHeroAssign;
    private System.Windows.Forms.CheckBox cbUnlimitedActionPoints;
    private System.Windows.Forms.TableLayoutPanel tlpBottom;
    private System.Windows.Forms.GroupBox gbResources;
    private System.Windows.Forms.GroupBox gbEmpireManagement;
    private System.Windows.Forms.GroupBox gbUnitsAndHeroes;
    private System.Windows.Forms.TableLayoutPanel tlpEmpireManagement;
    private System.Windows.Forms.FlowLayoutPanel flpEmpireManagement;
    private System.Windows.Forms.FlowLayoutPanel flpUnitsAndHeroes;
    private System.Windows.Forms.Button btnEditHeroes;
    private System.Windows.Forms.FlowLayoutPanel flpOperation;
    private System.Windows.Forms.CheckBox cbHeroExp;
    private System.Windows.Forms.CheckBox cbHeroAbilityLvMax;
    private System.Windows.Forms.CheckBox cbEnemyResources;
    private System.Windows.Forms.FlowLayoutPanel flpResources;
    private System.Windows.Forms.CheckBox cbCityPop;
    private System.Windows.Forms.SplitContainer splitContainer1;
    private System.Windows.Forms.Button btnReloadSaveList;
    private System.Windows.Forms.ListBox lbSaveFiles;
    private System.Windows.Forms.SplitContainer splitContainer2;
    private System.Windows.Forms.GroupBox gbSaveFile;
  }
}

