﻿using System;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace EndlessLegendSaveEditor
{
  internal static class Program
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    private static void Main()
    {
            try
            {
                // Write log, for debugging why app is not starting on some systems
                using (StreamWriter outfile = new StreamWriter(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "EndlessLegendSaveEditorLog.txt"), true))
                {
                    outfile.Write(System.DateTime.Now + " START"+ Environment.NewLine);
                }

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                // Set Culture Locale to en-US:
                CultureInfo culture = CultureInfo.CreateSpecificCulture("en-US"); ;

                CultureInfo.DefaultThreadCurrentCulture = culture;
                CultureInfo.DefaultThreadCurrentUICulture = culture;

                Thread.CurrentThread.CurrentCulture = culture;
                Thread.CurrentThread.CurrentUICulture = culture;

                Application.Run(new MainForm());

                using (StreamWriter outfile = new StreamWriter(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "EndlessLegendSaveEditorLog.txt"), true))
                {
                    outfile.Write(System.DateTime.Now + " STOP" + Environment.NewLine);
                }
            }
            catch (Exception ex) {
                using (StreamWriter outfile = new StreamWriter(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "EndlessLegendSaveEditorLog.txt"), true))
                {
                    outfile.Write(System.DateTime.Now + " ERROR: " + ex.Message.ToString() + Environment.NewLine);
                }
            }
    }
  }
}