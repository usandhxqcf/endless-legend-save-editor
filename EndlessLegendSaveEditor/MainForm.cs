﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Xml.XPath;

namespace EndlessLegendSaveEditor
{
    public partial class MainForm : Form
    {
        private static readonly string GameSaveFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\Endless Legend\Save Files\"; // Save files folder location
        private const string XMLFileRelativePath = "Endless Legend/Game.xml"; // Game data xml file location relative to save zip root
        private const string PlayerEmpireName = "Empire#0"; // Player empire name (for single player on slot 0)
        private static string GlobalZipPath = null;
        private XDocument GlobalGameDoc = null; // Stores Loaded Game data XML
        public XDocument locXML = null; // Stores Localization strings if available
        private XElement GlobalPlayerEmpireNode = null; // Points to XML node representing player empire
        private bool UseLocalXML = false;

        public MainForm() // Form constructor
        {
            InitializeComponent();
            LoadLocalization();
        }

        private void MainForm_Load(object sender, EventArgs e) // Called on finishing loading main form, initializes save list
        {
#if DEBUG
            UseLocalXML = true;
#endif
            //not elegant, but effective
            if (RefreshSaveList() > 0 || UseLocalXML)
            {
                InitializeOperationTarget();
            }
        }

        // "Refresh Save List" button pressed event
        private void btnReloadSaveList_Click(object sender, EventArgs e)
        {
            RefreshSaveList();
        }

        // Event launches when item in Save selection box has changed. Loads up Game.xml file from zip, sets player empire node that of "Empire#0", reads and sets changeable property values to edit boxes.
        private void lbSaveFiles_SelectedIndexChanged(object sender, EventArgs e)
        {
            InitializeOperationTarget((sender as ListBox).SelectedIndex);
        }

        private void InitializeOperationTarget(int saveItemIndex = 0)
        {
            //this.lbSaveFiles.SelectedIndex = saveItemIndex;
            InitializeGlobalDocAndNode((string)lbSaveFiles.SelectedItem);
            InitializeEmpireProperties(GlobalPlayerEmpireNode);
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns>save files count</returns>
        private int RefreshSaveList()
        {
            var directoryInfo = new DirectoryInfo(GameSaveFolder);
            if (!directoryInfo.Exists)
            { // Bail out if save path does not exist, leaving some info for user
                toolStripStatusLabel1.Text = "Save file folder not found!";
                return 0;
            }

            lbSaveFiles.Items.Clear();
            // Some linq magick i quite not understand, enumerates all zip files in a folder ordered by latest edited first
            foreach (string save in (from file in directoryInfo.GetFiles() where file.Extension.ToLower() == ".zip" orderby file.LastWriteTime descending select file.Name))
                lbSaveFiles.Items.Add(Path.GetFileNameWithoutExtension(save));

            // Inform user of save files found
            toolStripStatusLabel1.Text = "Found " + lbSaveFiles.Items.Count + " saves.";

            // Forces selection of first item, thus initiating XML parsing and reading property values
            if (lbSaveFiles.Items.Count > 0)
                lbSaveFiles.SelectedIndex = 0;

            return lbSaveFiles.Items.Count;
        }
        /// <summary>
        /// Tries to locate game english localization XML (assumes you have Steam version),
        /// if fails, locXML attribute remains 'null'. locXML attribute is used to look up hero names in hero edit window
        /// </summary>
        private void LoadLocalization()
        {
            RegistryKey regKey = Registry.CurrentUser;
            regKey = regKey.OpenSubKey(@"Software\Valve\Steam");

            if (regKey != null)
            {
                string installpath = regKey.GetValue("SteamPath").ToString();
                if (installpath != "")
                {
                    installpath += "/SteamApps/common/Endless Legend";
                    string locpath = installpath + "/Public/Localization/english/EF_Localization_Assets_Locales.xml";
                    try
                    {
                        locXML = XDocument.Load(locpath);
                    }
                    catch (System.IO.FileNotFoundException)
                    {
                        locXML = null;
                    }
                }
            }
            else return;
        }

        /// <summary>
        /// Initialize GlobalGameDoc and GlobalPlayerEmpireNode
        /// differs when compiled with debug or release option
        /// </summary>
        /// <param name="saveFileName"></param>
        private void InitializeGlobalDocAndNode(string saveFileName)
        {
            // This function needs reviewing, I think it might be rewritten more concisely
            StreamReader s = null;
            ZipArchive archive = null;
            if (UseLocalXML)
            {
                //use Game.xml in the same folder as program when using DEBUG compile option
                s = new StreamReader(@"Game.xml");
            }
            else
            {
                GlobalZipPath = GameSaveFolder + saveFileName + ".zip";
                // Loads and parses zipped XML file
                archive = ZipFile.OpenRead(GlobalZipPath);
                s = new StreamReader(archive.GetEntry(XMLFileRelativePath).Open());
            }
            GlobalGameDoc = XDocument.Load(s);
            GlobalPlayerEmpireNode = GetEmpireNode(GlobalGameDoc, PlayerEmpireName);
            s.Close();
            s.Dispose();
            if (archive != null) archive.Dispose(); // We do need dispose of archive, else I've got showstopping "file already open" on saving
        }

        private void InitializeEmpireProperties(XElement empireNode)
        {
            if (empireNode != null)
            {
                tbDust.Text = GetEmpireProperty(empireNode, "BankAccount").Value;
                tbEmpirePoints.Text = GetEmpireProperty(empireNode, "EmpirePointStock").Value;
                tbResearchStock.Text = GetEmpireProperty(empireNode, "EmpireResearchStock").Value;
                tbPeacePoints.Text = GetEmpireProperty(empireNode, "EmpirePeacePointStock").Value;
            }
        }

        // Sets plEmpireNode variable to player empires XML node, empirename - passes Player empire name, usually "Empire#0"
        private XElement GetEmpireNode(XDocument gameDoc, string empireName)
        {
            return gameDoc.XPathSelectElement("/GameManager/Game/Empires/MajorEmpire[@Name='" + empireName + "']");
        }

        private IEnumerable<XElement> GetNonPlayerEmpireNodes(XDocument gameDoc, string playerEmpireName)
        {
            return gameDoc.XPathSelectElements("/GameManager/Game/Empires/MajorEmpire[@Name!='" + playerEmpireName + "']");
        }

        /// <summary>
        /// returns empires property XML Node by name passed in propName
        /// </summary>
        /// <param name="empireNode"></param>
        /// <param name="propName"></param>
        /// <returns></returns>
        private XElement GetEmpireProperty(XElement empireNode, string propName)
        {
            return empireNode.XPathSelectElement("SimulationObject/Properties/Property[@Name='" + propName + "']");
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="empire"></param>
        /// <param name="bankAccount"></param>
        /// <param name="empirePointStock"></param>
        /// <param name="empireResearchStock"></param>
        /// <param name="empirePeacePointStock"></param>
        /// <param name="lowestFilter">for reconize enemy cheat resources, if >= 20000 set it to 0</param>
        private void SetEmpireMainResources(XElement empire,
            string bankAccount = "0", string empirePointStock = "0", string empireResearchStock = "0", string empirePeacePointStock = "0", float lowestFilter = 20000)
        {
            float floatCache = 0;
            if (float.TryParse(GetEmpireProperty(empire, "BankAccount").Value, out floatCache) && floatCache >= lowestFilter)
            { GetEmpireProperty(empire, "BankAccount").Value = bankAccount; }
            if (float.TryParse(GetEmpireProperty(empire, "EmpirePointStock").Value, out floatCache) && floatCache >= lowestFilter)
            { GetEmpireProperty(empire, "EmpirePointStock").Value = empirePointStock; }
            if (float.TryParse(GetEmpireProperty(empire, "EmpireResearchStock").Value, out floatCache) && floatCache >= lowestFilter)
            { GetEmpireProperty(empire, "EmpireResearchStock").Value = empireResearchStock; }
            if (float.TryParse(GetEmpireProperty(empire, "EmpirePeacePointStock").Value, out floatCache) && floatCache >= lowestFilter)
            { GetEmpireProperty(empire, "EmpirePeacePointStock").Value = empirePeacePointStock; }
        }

        private void SetEmpireStrageticResources(XElement empire, string strategic = "0", float lowestFilter = 800)
        {
            // Regexps for matching resource properties
            var rxStrat = new Regex("^Strategic\\d+Stock$", RegexOptions.IgnoreCase);

            var eprops = empire.XPathSelectElements("SimulationObject/Properties/Property"); // Property list query
            // Set Strategic resources to 999
            foreach (XElement stratRes in (from el in eprops where rxStrat.IsMatch(el.Attribute("Name").Value) select el))
            {
                if (float.Parse(stratRes.Value) >= lowestFilter)
                {
                    stratRes.Value = strategic;
                }
            }
        }

        private void SetEmpireLuxuryResources(XElement empire, string luxury = "0", float lowestFilter = 800)
        {
            // Regexps for matching resource properties
            var rxLuxury = new Regex("^Luxury\\d+Stock$", RegexOptions.IgnoreCase);

            var eprops = empire.XPathSelectElements("SimulationObject/Properties/Property"); // Property list query

            // Set Luxury resources to 999
            foreach (XElement luxuryRes in (from el in eprops where rxLuxury.IsMatch(el.Attribute("Name").Value) select el))
            {
                if (float.Parse(luxuryRes.Value) >= lowestFilter)
                {
                    luxuryRes.Value = luxury;
                }
            }
        }

        // Event, launches on "Update Save" button click, updates values in XML and saves to Zip save file, also makes a backup if option is selected
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                // Validate inputs
                float f;
                if (!float.TryParse(tbDust.Text, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out f) ||
                  !float.TryParse(tbEmpirePoints.Text, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out f) ||
                  !float.TryParse(tbResearchStock.Text, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out f) ||
                  !float.TryParse(tbPeacePoints.Text, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out f))
                {
                    toolStripStatusLabel1.Text = "Incorrect input.";
                    return;
                }

                if (!UseLocalXML)
                {
                    // Make backup
                    if (cbBackup.Checked)
                        File.Copy(GlobalZipPath, GlobalZipPath + ".bak", true);
                }

                #region Resources

                SetEmpireMainResources(GlobalPlayerEmpireNode, tbDust.Text, tbEmpirePoints.Text, tbResearchStock.Text, tbPeacePoints.Text, -1);

                if (cbStrategicResources.Checked)
                {
                    SetEmpireStrageticResources(GlobalPlayerEmpireNode, "999", -1);
                }

                if (cbLuxuryResources.Checked)
                {
                    SetEmpireLuxuryResources(GlobalPlayerEmpireNode, "999", -1);
                }

                if (cbEnemyResources.Checked)
                {
                    IEnumerable<XElement> nonPlayerEmpires = GetNonPlayerEmpireNodes(GlobalGameDoc, PlayerEmpireName);
                    foreach (XElement nonPlayerEmpire in nonPlayerEmpires)
                    {
                        SetEmpireMainResources(nonPlayerEmpire);
                        SetEmpireStrageticResources(nonPlayerEmpire);
                        SetEmpireLuxuryResources(nonPlayerEmpire);
                    }
                }

                #endregion Resources

                #region DepartmentOfPlanificationAndDevelopment

                if (cbBoostersUp.Checked) // Set all available boosters to last 600 turns
                {
                    foreach (XElement booster in GlobalPlayerEmpireNode.XPathSelectElements("Agencies/DepartmentOfPlanificationAndDevelopment/Boosters/Booster"))
                    {
                        try
                        {
                            int bDuration = Convert.ToInt32(booster.Attribute("Duration").Value);
                            int bRemaining = Convert.ToInt32(booster.Attribute("RemainingTime").Value);
                            booster.Attribute("Duration").Value = "600";
                            booster.Attribute("RemainingTime").Value = (bRemaining - bDuration + 600).ToString();
                        }
                        catch (FormatException)
                        {
                            Debug.WriteLine("Error parsing Booster durations, values is not integer");
                        }
                    }
                }

                if (cbInstantPlan.Checked)
                {
                    XElement empirePlanChoiceRemainingTurn = GlobalPlayerEmpireNode.XPathSelectElement("Agencies/DepartmentOfPlanificationAndDevelopment/EmpirePlans/EmpirePlanChoiceRemainingTurn");
                    empirePlanChoiceRemainingTurn.Value = "0";
                }

                #endregion DepartmentOfPlanificationAndDevelopment

                #region DepartmentOfTheInterior

                if (cbCityPop.Checked)
                {
                    foreach (XElement city in GlobalPlayerEmpireNode.XPathSelectElement("Agencies/DepartmentOfTheInterior/Cities").Descendants("City"))
                    {
                        //there can be some cities without this property, perhaps needs at least one time of population growth in game
                        XElement pop = city.XPathSelectElement("SimulationObject/Properties/Property[@Name='Population']");
                        if (pop != null)
                        {
                            pop.Value = "99";
                        }
                        XElement growth = city.XPathSelectElement("SimulationObject/Properties/Property[@Name='CityGrowthStock']");
                        if (growth != null)
                        {
                            growth.Value = "99999999";
                        }
                    }
                }

                if (cbInstantPlan.Checked)
                {
                    XElement empirePlanChoiceRemainingTurn = GlobalPlayerEmpireNode.XPathSelectElement("Agencies/DepartmentOfPlanificationAndDevelopment/EmpirePlans/EmpirePlanChoiceRemainingTurn");
                    empirePlanChoiceRemainingTurn.Value = "0";
                }

                #endregion DepartmentOfTheInterior

                #region Research

                if (cbResearchProgress.Checked)
                {
                    var allQueuedResearchStock = GlobalPlayerEmpireNode.XPathSelectElement("Agencies/DepartmentOfScience/Queue/Researches").Descendants("Stock")
                        .Where(s => s.Attribute("PropertyName").Value == "EmpireResearch");
                    foreach (XAttribute attribute in (from stock in allQueuedResearchStock
                                                      select stock.Attribute("Value")))
                        attribute.Value = "99999990";
                }

                #endregion Research

                #region Units and Heroes

                if (cbHealAllArmies.Checked)
                { // Heal All Armies
                    foreach (XElement unitHealth in (from unit in GetAllUnitsAndHeroes(GlobalPlayerEmpireNode)
                                                     select unit.XPathSelectElement("SimulationObject/Properties/Property[@Name='Health']")))
                    {
                        unitHealth.Value = "1";
                    }
                }

                if (cbUnlimitedActionPoints.Checked)
                {
                    foreach (XElement ap in (from unit in GetAllUnitsAndHeroes(GlobalPlayerEmpireNode)
                                             select unit.XPathSelectElement("SimulationObject/Properties/Property[@Name='MaximumNumberOfActionPoints']")))
                        if (ap != null)
                        {
                            ap.Value = "99990";
                        }
                }

                if (cbUnlimitedMovementForever.Checked)
                {
                    foreach (XElement move in (from unit in GetAllUnitsAndHeroes(GlobalPlayerEmpireNode)
                                               select unit.XPathSelectElement("SimulationObject/Properties/Property[@Name='Movement']")))
                    {
                        if (move != null)
                        {
                            move.Value = "999990";
                        }
                    }
                }

                if (cbUnlimitedMovementOneTurn.Checked)
                {
                    foreach (XElement moveBonus in (from unit in GetAllUnitsAndHeroes(GlobalPlayerEmpireNode)
                                                    select unit.XPathSelectElement("SimulationObject/Properties/Property[@Name='MovementBonus']")))
                    {
                        if (moveBonus != null)
                        {
                            moveBonus.Value = "999990";
                        }
                    }
                }

                if (cbResetSpentMovement.Checked)
                {
                    foreach (XElement unitSpentMoves in (from unit in GetAllUnitsAndHeroes(GlobalPlayerEmpireNode)
                                                         select unit.XPathSelectElement("SimulationObject/Properties/Property[@Name='SpentMovement']")))
                    {
                        if (unitSpentMoves != null)
                        {
                            unitSpentMoves.Value = "0";
                        }
                    }
                }

                if (cbHeroExp.Checked)
                {
                    foreach (XElement heroExp in (from unit in GetAllHeroes(GlobalPlayerEmpireNode)
                                                  select unit.XPathSelectElement("SimulationObject/Properties/Property[@Name='Experience']")))
                    {
                        if (heroExp != null)
                        {
                            heroExp.Value = "20000";
                        }
                    }
                }

                if (cbHeroSkillPoint.Checked)
                {
                    foreach (XElement skillPoint in (from unit in GetAllHeroes(GlobalPlayerEmpireNode)
                                                     select unit.XPathSelectElement("SimulationObject/Properties/Property[@Name='MaximumSkillPoints']")))
                    {
                        if (skillPoint != null)
                        {
                            skillPoint.Value = "120";
                        }
                    }
                }

                if (this.cbHeroAbilityLvMax.Checked)
                {
                    //Abilities level and Tags are fake, only defines the max level
                    //Descriptors are truely working
                    //ReferenceCount is also dummy...
                    //can't find what affects interface display
                    foreach (XElement unit in GetAllHeroes(GlobalPlayerEmpireNode))
                    {
                        string endPattern = @"Descriptor\d+";
                        XElement tags = unit.XPathSelectElement("SimulationObject/Tags");
                        StringBuilder tagsBuilder = new StringBuilder(tags.Value);
                        //XElement abilitiesNode = unit.XPathSelectElement("Abilities");
                        foreach (XElement ability in unit.XPathSelectElement("Abilities").Descendants("Ability"))
                        {
                            //need the name to match descriptor and tag
                            string abilityName = ability.Attribute("Name").Value;
                            //the ReferenceCount value in "Level" node must be the same as this
                            //for some abilities such as xxx slayer abilities, the ReferenceCount will change with different level, level 0~2 ReferenceCount = 1, level 3(displayed as 4 in game) ReferenceCount = 2
                            string referenceCount = ability.Attribute("ReferenceCount").Value;

                            int levelCount = int.Parse(ability.XPathSelectElement("Levels").Attribute("Count").Value);
                            int maxLevel = levelCount - 1;

                            if (maxLevel > 0)// >= 0 will have the same result
                            {
                                //update Level's ReferenceCount for display
                                IEnumerable<XElement> orderedLevels = ability.XPathSelectElement("Levels").Descendants("Level").OrderBy(l => l.Attribute("Number").Value);
                                foreach (XElement level in orderedLevels)
                                {
                                    //clear every level
                                    level.Attribute("ReferenceCount").Value = "0";
                                }
                                //set the level with biggest Number
                                orderedLevels.Last().Attribute("ReferenceCount").Value = referenceCount;

                                //find Descriptor
                                XAttribute descriptorNameAttribute = unit.XPathSelectElement("SimulationObject/Descriptors").Descendants("Descriptor")
                                    .SingleOrDefault(d => Regex.IsMatch(d.Attribute("Name").Value, abilityName + endPattern)).Attribute("Name");

                                string replacement = abilityName + "Descriptor" + levelCount;

                                //update Tag just to ensure there won't be future unintended behavior
                                tagsBuilder.Replace(descriptorNameAttribute.Value, replacement);

                                //update Descriptor for effect
                                descriptorNameAttribute.Value = replacement;

                                //update Ability just to ensure there won't be future unintended behavior
                                ability.Attribute("Level").Value = maxLevel.ToString();
                            }
                        }
                        tags.Value = tagsBuilder.ToString();
                    }
                }

                if (cbUnlockHeroAssign.Checked)
                {
                    foreach (XElement assignmentCooldown in (from unit in GetAllHeroes(GlobalPlayerEmpireNode)
                                                             select unit.XPathSelectElement("SimulationObject/Properties/Property[@Name='AssignmentCooldown']")))
                    {
                        if (assignmentCooldown != null)
                        {
                            assignmentCooldown.Value = "5";
                        }
                    }
                }

                #endregion Units and Heroes

                //write changes to save file
                if (UseLocalXML)
                {
                    using (StreamWriter writer = new StreamWriter(@"Game.xml"))
                    {
                        GlobalGameDoc.Save(writer); // Write changed to Game.xml in program base directory
                    }
                }
                else
                {
                    // Zip Changes
                    using (ZipArchive archive = ZipFile.Open(GlobalZipPath, ZipArchiveMode.Update))
                    {
                        archive.GetEntry(XMLFileRelativePath).Delete(); // Delete old Game.xml file
                        using (StreamWriter writer = new StreamWriter(archive.CreateEntry(XMLFileRelativePath).Open()))
                        {
                            GlobalGameDoc.Save(writer); // Write changed Game.xml to savegame zip
                        }
                    }
                }

                // Inform user of successful update
                toolStripStatusLabel1.Text = "Save \"" + lbSaveFiles.SelectedItem + "\" updated.";
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine(ex.Message).AppendLine(ex.StackTrace);
                if (ex.InnerException != null)
                {
                    sb.AppendLine(ex.InnerException.Message).AppendLine(ex.InnerException.StackTrace);
                }
                sb.AppendLine();

                File.AppendAllText("Log.txt", sb.ToString(), Encoding.UTF8);
            }
        } // button1_Click -- Ends

        /// <summary>
        /// GetAllUnitsAndHeroes, except those in converted minor faction villages
        /// </summary>
        /// <param name="empireNode"></param>
        /// <returns></returns>
        private IEnumerable<XElement> GetAllUnitsAndHeroes(XElement empireNode)
        {
            //failure, can't use more than one level in Descendants
            //var allUnitsInArmies = plEmpireNode.XPathSelectElement("Agencies/DepartmentOfDefense/Armies").Descendants("Units/Unit");

            var allUnitsAndHeroesInArmies = GlobalPlayerEmpireNode.XPathSelectElement("Agencies/DepartmentOfDefense/Armies").Descendants("Unit");
            //lack units in converted minor race village (located under MinorEmpire/Agencies/BarbarianCouncil/Villages node), user should pull them out into new army
            var allUnitsAndHeroesInCities = GlobalPlayerEmpireNode.XPathSelectElement("Agencies/DepartmentOfTheInterior/Cities").Descendants("Unit");
            //descriptor filter tested success
            //var allUnitsInArmies = allUnitsAndHeroesInArmies.Where<XElement>(p => p.XPathSelectElement("SimulationObject/Descriptors/Descriptor[@Name='UnitHero']") == null);
            //var allHeroesInArmies = allUnitsAndHeroesInArmies.Where<XElement>(p => p.XPathSelectElement("SimulationObject/Descriptors/Descriptor[@Name='UnitHero']") != null);
            var unassignedHeroes = GlobalPlayerEmpireNode.XPathSelectElement("Agencies/DepartmentOfEducation/UnassignedHeroes").Descendants("Unit");

            var allUnitsAndHeroes = allUnitsAndHeroesInArmies.Union(unassignedHeroes).Union(allUnitsAndHeroesInCities);

            return allUnitsAndHeroes;
        }

        /// <summary>
        /// this function filters result from GetAllUnitsAndHeroes
        /// </summary>
        /// <param name="empireNode"></param>
        /// <returns></returns>
        private IEnumerable<XElement> GetAllHeroes(XElement empireNode)
        {
            return GetAllUnitsAndHeroes(empireNode).Where<XElement>(p => p.XPathSelectElement("SimulationObject/Descriptors/Descriptor[@Name='UnitHero']") != null);
        }

        /// <summary>
        /// Helper function, searhes for Hero "Unit" nodes (this node stores values like XP, MaximumSkillPoints, etc.) under specified <anode>.
        /// If finds some, searches for it's "UnitDesign" node (which stores hero name and some other data)
        /// Unit node attribute "UnitDesignModel" and UnitDesign node attribute "Model" are linked (must match), this way two of them are recognised as a pair
        /// Then creates new HeroEntry object pointing to those two nodes and appends it to a list
        /// HeroEntry class is defined in HeroEditForm.cs
        /// </summary>
        /// <param name="anode"></param>
        /// <param name="heroList"></param>
        private void addHeroesInNode(XElement anode, IList<HeroEntry> heroList)
        {
            foreach (XElement herounit in (from unit in anode.Descendants("Unit") where unit.Element("Rank").Attribute("Name").Value == "UnitRankHero" select unit))
            {
                XElement herodesign = GlobalPlayerEmpireNode.XPathSelectElement("Agencies/DepartmentOfDefense/UnitDesigns/Hidden/UnitDesign[@Model='" + herounit.Attribute("UnitDesignModel").Value + "']");
                heroList.Add(new HeroEntry(herodesign, herounit, locXML));
            }
        }

        private void btnEditHeroes_Click(object sender, EventArgs e)
        {
            // Make Empty Hero List:
            //List < HeroEntry > heroList = new List<HeroEntry>();
            List<HeroEntry> heroList = new List<HeroEntry>();

            // Hero data entries can be in three places: Armies, Cities and unassigned, fetch them all
            //addHeroesInNode(GlobalPlayerEmpireNode.XPathSelectElement("Agencies/DepartmentOfDefense/Armies"), heroList); // Get Heroes in armies
            //addHeroesInNode(GlobalPlayerEmpireNode.XPathSelectElement("Agencies/DepartmentOfTheInterior/Cities"), heroList); // Get Heroes in Cities
            //addHeroesInNode(GlobalPlayerEmpireNode.XPathSelectElement("Agencies/DepartmentOfEducation/UnassignedHeroes"), heroList); // Get Unassigned Heroes

            foreach (XElement hero in GetAllHeroes(GlobalPlayerEmpireNode))
            {
                XElement heroDesign = GlobalPlayerEmpireNode.XPathSelectElement("Agencies/DepartmentOfDefense/UnitDesigns/Hidden/UnitDesign[@Model='" + hero.Attribute("UnitDesignModel").Value + "']");
                heroList.Add(new HeroEntry(heroDesign, hero, locXML));
            }

            HeroEditForm heroForm = new HeroEditForm(heroList);
            // Update Hero List:
            //HeroForm.SetHeroList(heroList);

            if (heroForm.ShowDialog() == DialogResult.OK) // Display Hero Editing form in modal mode
            {
                if (heroForm.HeroDataChanged()) toolStripStatusLabel1.Text = "Heroes edited, press \"Update Save\""; // Inform user if something was changed using the form
            }
        }

    }
}